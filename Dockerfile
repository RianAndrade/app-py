FROM python:alpine3.19

WORKDIR /app


COPY app.py .


EXPOSE 80



RUN rm -rf /var/lib/apt/lists/* /root/.cache/pip


CMD ["python", "app.py"]
