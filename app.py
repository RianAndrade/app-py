from http.server import BaseHTTPRequestHandler, HTTPServer
import sys

class SimpleHTTPRequestHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()
        self.wfile.write(b'Hello World, Boa Tarde! Jack!')

def run(port):
    server_address = ('', 80)
    httpd = HTTPServer(server_address, SimpleHTTPRequestHandler)
    print(f'Servidor rodando na porta 80...')
    httpd.serve_forever()

if __name__ == '__main__':
    port = int(sys.argv[1]) if len(sys.argv) > 1 else 8000
    run(port)
